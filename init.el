
;;----------------------------------------------------------------------
;; PACKAGE MANAGEMENT
;;----------------------------------------------------------------------

(require 'package)
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/")
	     '("org" . "http://orgmode.org/elpa/"))
(package-initialize)

;;----------------------------------------------------------------------
;; THEMING
;;----------------------------------------------------------------------

(load-theme 'tango-dark)

(add-to-list 'default-frame-alist '(font . "Inconsolata-14"))

(menu-bar-mode -1)

(tool-bar-mode -1)

(set-scroll-bar-mode 'nil)

;;verstecke den Emacs Startbildschirm
(setq inhibit-startup-screen t)

;;----------------------------------------------------------------------
;; CUSTOM SET VARIABLES
;;----------------------------------------------------------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(global-magit-file-mode nil)
 '(magit-auto-revert-mode t)
 '(magit-git-executable "C:\\Program Files\\Git\\bin\\git.exe")
 '(magit-wip-after-apply-mode nil)
 '(magit-wip-after-save-mode nil)
 '(magit-wip-before-change-mode nil)
 '(org-emphasis-alist
   (quote
    (("*" bold)
     ("/" bold)
     ("_" underline)
     ("=" org-verbatim verbatim)
     ("~" org-code verbatim)
     ("+"
      (:strike-through t)))))
 '(org-footnote ((t (:foreground "grey"))))
 '(org-footnote-auto-label nil)
 '(org-footnote-define-inline (quote fninline))
 '(org-ref-colorize-links t)
 '(org-ref-default-citation-link "citet")
 '(org-ref-label-color "dark green")
 '(org-ref-label-face ((t (:inherit org-link :foreground "dark green"))))
 '(vc-git-program "C:\\Program Files\\Git\\bin\\git.exe"))
 '(package-selected-packages
   (quote
    (ob-php ob-browser org-bullets magit smex powerline ace-window ebib web-mode indium org-ref org-plus-contrib org)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))

;;----------------------------------------------------------------------
;; ALLGEMENEINE EINSTELLUNGEN
;;----------------------------------------------------------------------
(setq default-directory "~/Documents/")

(setq user-full-name "Stefan Boehringer"
      user-mail-address "stefan.boehringer@posteo.de")

(setq make-backup-files nil) ;; stop creating those backup~ files
(setq auto-save-default nil) ;; stop creating those #autosave# files

;; ignoriere alle audiovisuellen Warnungen
(setq ring-bell-function 'ignore)

;; ausgewählten Text bei Eingabe löschen
(delete-selection-mode 1) 

;; keine "yes-or-no"-Fragen - "y-or-n" reicht aus
(defalias 'yes-or-no-p 'y-or-n-p)

;; Zusammengehörende Klammern hervorheben
(show-paren-mode 1)

;; Text zwischen den Klammern nicht hervorheben
(setq show-paren-style 'parenthesis)

;; Automatisch zugehörige Klammer setzen
(electric-pair-mode 1)

;; Aktuelle Spalten- und Zeilennummer in der Mode Line anzeigen
(column-number-mode nil)
(line-number-mode 1)

;;Kalender soll mit Montag beginnen und europäisches Datumsformat schlucken (Tag, Monat, Jahr)
(setq calendar-week-start-day 1)
(setq calendar-date-style 'european)

;; Rechtschreibprüfung mit aspell statt ispell
(add-to-list 'exec-path "C:/Program Files (x86)/Aspell/bin/")
(setq-default ispell-program-name "aspell")
(require 'ispell)

;; Deutsche Rechtschreibung falls \usepackage{ngerman}
;; oder german benutzt wird
(add-hook 'TeX-language-de-hook
(function (lambda () (ispell-change-dictionary "german8"))))

;; ignoriere Groß- und Kleinschreibung bei der Autovervollständigung von Dateinamen
(setq read-file-name-completion-ignore-case t)

;; Ein einfaches Leerzeichen nach Punkt beendet einen Satz!
(setq sentence-end-double-space nil)

;; turn on abbrev mode globally
(setq-default abbrev-mode t)
(setq save-abbrevs 'silently)

;; use iBuffer as default buffer-list
(defalias 'list-buffers 'ibuffer)

;; save bookmark when emacs quits
(setq bookmark-save-flag t)

;; Benutze Visual-Line-Mode so oft wie möglich
(add-hook 'mail-mode-hook 'turn-on-visual-line-mode)
(add-hook 'message-mode-hook 'turn-on-visual-line-mode)
(visual-line-mode t)
(global-visual-line-mode t)

;; Bootstrap `use-package'
;; Pakete werden in der init.el mit Usepackage verwaltet
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; per c-x o und Nummer in ein Fenster wechseln
(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))))

;; eine hübschere Modline
(use-package powerline
  :ensure t)
(require 'powerline)
(powerline-default-theme)

;; schönere Ergebnisse mit M-x im Zusammenspiel von ido-mode und smex
(ido-mode t)
(setq ido-enable-flex-matching t) ;; fuzzy matching is a must have
(setq ido-enable-last-directory-history t) ;; erinnere dich an meine meistgenutzten Verzeichnisse

(use-package smex
  :ensure t
  :bind (("M-x" . smex))
  :config (smex-initialize))

;; SMEX
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command) ;; This is your old M-x:

;; MAGIT
(use-package magit
  :ensure t)
(global-set-key (kbd "C-x g") 'magit-status)
(setenv "GIT_ASKPASS" "git-gui--askpass")

;; EBIB
(setq ebib-notes-use-single-file "~/Documents/org/lit-notes.org")
(setq ebib-preload-bib-files (quote ("~/Documents/org/master.bib")))

;; YASNIPPET
(require 'yasnippet)
(yas-global-mode 1)

;; COMPANY MODE
(add-hook 'after-init-hook 'global-company-mode)
(require 'company-auctex)
(company-auctex-init)
(require 'company-bibtex)
(add-to-list 'company-backends 'company-bibtex)
  (setq company-bibtex-bibliography
	'("~/Documents/org/master.bib"))

(require 'web-mode)
(require 'php-mode)

;;----------------------------------------------------------------------
;; ORG-MODE
;;----------------------------------------------------------------------

(use-package org
  :mode (("\\.\\(org\\|org_archive\\)$" . org-mode))
  :ensure org-plus-contrib
  :config 
  (progn
    (setq org-hide-leading-stars 'hidestars)
    (setq org-return-follows-link t)
    (setq org-drawers (quote ("PROPERTIES" "CLOCKTABLE" "LOGBOOK" "CLOCK")))
    (setq org-completion-use-ido t)
    (setq org-tags-exclude-from-inheritance '("review"))
    ;;benutze kein hyperref
    (setq org-latex-hyperref-template t)
    ;; schütze vor der versehentlichen Löschung eines Subtrees
    (setq org-ctrl-k-protect-subtree t) 
    (setq org-catch-invisible-edits t)
    ;; blank lines before new headings
    (setq org-blank-before-new-entry
      '((heading . always)
       (plain-list-item . nil)))
    (setq org-return-follows-link t)
    ;; leave an empty line between folded subtrees
    (setq org-cycle-separator-lines 1)
    ;;Stop accidentally highlighting org-mode links all the time:
    (setq mouse-highlight nil)
    (setq cursor-in-non-selected-windows nil) 
    (setq org-startup-folded t)
    (setq org-indent-mode nil)))

(setq org-capture-templates
      '(("t" "Aufgabe in tasks.org" entry (file+headline "~/Documents/org/tasks.org" "Inbox")
         "* TODO %?")
        ("w" "Waiting For Reply (Mail)" entry (file+headline "~/Documents/org/tasks.org" "Inbox") 
         "* WAITING Antwort auf %a")))

;; Ein "!" bedeutet Zeitstempel
;; Ein "@" bedeutet Notiz
(setq org-todo-keywords
 '((sequence "TODO(t)" "STARTED(s!)" "WAITING(w@/!)" "TERMIN(a)" "PROJ(p)" "NOTIZ(n)" "BESPROCHEN(b)"
             "DELEGATED(g@/!)" "|" "DONE(d!)" "CANCELED(c@)")))

;; Fast TODO Selection
(setq org-use-fast-todo-selection t)

;; ORG LATEX
;; Footnotes should be inline and anonymous (LaTeX style)
(setq org-footnote-define-inline 'fninline)
(setq org-footnote-auto-label nil)
(require 'ox-extra)
(ox-extras-activate '(ignore-headlines))

;; org-export use KOMA-Script class arcticle as export
(require 'ox-latex)
(with-eval-after-load 'ox-latex
   (add-to-list 'org-latex-classes
          '("koma-article"
             "\\documentclass{scrartcl}"
             ("\\section{%s}" . "\\section*{%s}")
             ("\\subsection{%s}" . "\\subsection*{%s}")
             ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
             ("\\paragraph{%s}" . "\\paragraph*{%s}")
             ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(add-to-list 'org-structure-template-alist
             '("q" "\n#+BEGIN_QUOTE\n?\n#+END_QUOTE\n"))
(add-to-list 'org-structure-template-alist
             '("i" "\n#+BEGIN_ITEMIZE\n \\item ?\n#+END_ITEMIZE\n"))
(add-to-list 'org-structure-template-alist
             '("y" "#+BEGIN_COMMENT\n?\n#+END_COMMENT"))
;; ORG-REF
(require 'org-ref)
(setq reftex-default-bibliography '("~/Documents/org/master.bib"))

;; see org-ref for use of these variables
(setq org-ref-bibliography-notes "~/Documents/org/lit-notes.org"
      org-ref-default-bibliography '("~/Documents/org/master.bib")
      org-ref-pdf-directory "~/Documents/")

(setq bibtex-completion-bibliography "~/Documents/org/master.bib")

;; ORG PDF Export (nur für Unix, nicht unter Windows)
;;(setq org-latex-pdf-process
;;      '("latexmk -f -pdf %f"))

;; ORG AGENDA
(global-set-key (kbd "C-c a") 'org-agenda)
;; Aktuelle Zeile in der Agenda hervorheben
(add-hook 'org-agenda-mode-hook '(lambda () (hl-line-mode 1 )))

;; Tasks mit Datum in der Agenda ausblenden, wenn sie bereits erledigt sind:
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)

;; Agenda in der Zwei-Tagesansicht anzeigen
(setq org-agenda-span 2)

(setq org-agenda-window-setup (quote current-window));;öffne agenda in gleichem Fenster
(setq org-agenda-tags-column -100) ; take advantage of the screen width
(setq org-agenda-sticky nil)
(setq org-agenda-use-tag-inheritance t)
(setq org-agenda-show-log t)

(setq org-agenda-files (quote 
			("~/Documents/org/tasks.org")))

;; ORG-BABEL
;;(use-package org-babel
;;  :init
;;  (org-babel-do-load-languages
;;   'org-babel-load-languages
;;   '((emacs-lisp . t)
;;     (python . t)
;;     (shell . t)
;;     (js . t)
;;     (latex . t)
;;     (org . t)
;;     (dot . t)
;;     (gnuplot . t)
;;     (ruby . t)
;;     (screen . nil)
;;     (ledger . t)
;;     (C . t)
;;     (sql . t)
;;     (css . t)
;;     (php . t))))

(setq org-src-fontify-natively nil)

(require 'ox-odt)
(setq org-odt-preferred-output-format "docx")

;;----------------------------------------------------------------------
;; AUCTEX
;;----------------------------------------------------------------------

(require 'tex)
(TeX-global-PDF-mode t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

;;----------------------------------------------------------------------
;; WEB-MODE
;;----------------------------------------------------------------------

(use-package web-mode
  :ensure t
  :config
  (progn
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (setq web-mode-engines-alist
          '(("django"    . "\\.html\\'")))
    (setq web-mode-ac-sources-alist
          '(("css" . (ac-source-css-property))
            ("html" . (ac-source-words-in-buffer ac-source-abbrev))))
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-code-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-enable-current-element-highlight t)
    (setq web-mode-enable-auto-closing t)
    (setq web-mode-enable-auto-quoting t)
    (setq web-mode-enable-auto-pairing t)
    (setq web-mode-enable-auto-expanding t)
    (setq web-mode-enable-css-colorization t)))
